function align_lasso(direction){

    d3.selectAll('circle').remove();
    d3.selectAll('#lineHint').remove();
    d3.selectAll('.lasso_box').remove();

    let align_rect_set = new Map();

    if(shift_chosed_list.size>1)
        align_rect_set = shift_chosed_list;
    else
        align_rect_set = lasso_rect_set;


    if(align_rect_set.size == 0)
    {
        alert("Please select Rectangles to be aligned first!");
        return;
    }

    var arrayObj_v= Array.from(align_rect_set);
    var arrayObj_h = Array.from(align_rect_set);
    arrayObj_v.sort(function(a,b){return a[1].top-b[1].top})
    arrayObj_h.sort(function(a,b){return a[1].left-b[1].left})
    var reference_top_id,reference_top_rect;
    var reference_left_id,reference_left_rect;

    var size = align_rect_set.size;
    reference_top_id = arrayObj_v[0][0]
    reference_top_rect = arrayObj_v[0][1];
    reference_left_id = arrayObj_h[0][0]
    reference_left_rect = arrayObj_h[0][1]
     var sorted_rect_set;
    if(direction == "top" || direction == 'bottom')
        sorted_rect_set = new Map(arrayObj_h);
    else
        sorted_rect_set = new Map(arrayObj_v);

    arrayObj_v.sort(function(a,b){return a[1].bottom-b[1].bottom})
    arrayObj_h.sort(function(a,b){return a[1].right-b[1].right})
    var reference_bot_id,reference_bot_rect;
    var reference_right_id,reference_right_rect;

    reference_bot_id = arrayObj_v[size-1][0]
    reference_bot_rect = arrayObj_v[size-1][1];
    reference_right_id = arrayObj_h[size-1][0]
    reference_right_rect = arrayObj_h[size-1][1]



    let reference_id_v, reference_rect_v;

    var startX =0;
    var startY =0;
    if(direction == 'top')
    {

        startY = reference_top_rect.top
    }
    if(direction == 'bottom')
    {

         startY = reference_bot_rect.bottom
    }
    if(direction == 'left')
    {

        startX = reference_left_rect.left
    }
    if(direction == 'right')
    {

        startX = reference_right_rect.right
    }


    var aRectShape;
    var newRectPosition = new Map();

    if(direction == 'top'){

        sorted_rect_set.forEach((value,key,self) => {

             aRectShape = new RectShape(value.left,startY,value.right,startY+value.bottom-value.top,true);
             newRectPosition.set(key,aRectShape);


             d3.select('#' + key).attr('y', startY);

             d3.select('#' + key + 'R').attr('y', startY);
             updateSmLine(key);
        });
    }
    if(direction == 'bottom'){

        sorted_rect_set.forEach((value,key,self) => {
             aRectShape = new RectShape(value.left,startY - value.bottom + value.top,value.right,startY,true)
             newRectPosition.set(key,aRectShape)
             d3.select('#' + key).attr('y', startY - value.bottom + value.top);
             d3.select('#' + key + 'R').attr('y', startY - value.bottom + value.top);
             updateSmLine(key);
        });
    }
    if(direction == 'left'){
        sorted_rect_set.forEach((value,key,self) => {
             aRectShape = new RectShape(startX,value.top,startX+value.right-value.left,value.bottom,true);
             newRectPosition.set(key,aRectShape)

             d3.select('#' + key).attr('x', startX);
             d3.select('#' + key +'R').attr('x', startX);
            updateSmLine(key);
        });
    }
    if(direction == 'right'){
        sorted_rect_set.forEach((value,key,self) => {
             aRectShape = new RectShape(startX-value.right+value.left,value.top,startX,value.bottom,true);
             newRectPosition.set(key,aRectShape);

             d3.select('#' + key).attr('x', startX-value.right+value.left,);
             d3.select('#' + key + 'R').attr('x', startX-value.right+value.left,);
             updateSmLine(key);
        });
    }

    align_rect_set.clear();
    shift_chosed_list.clear();
    lasso_rect_set.clear()

    
     if(shift_chosed_list.size>1)
        shift_chosed_list = newRectPosition;
    else
        lasso_rect_set = newRectPosition;

    newRectPosition.forEach((value,key,self) => {
        rectPosition.set(key,value)
        if(shift_down)
            drawEightCircles(key);
    });
    initPosition = [];
    for(let m = 0;m<d3.selectAll('rect')[0].length;m++){
        let p = [];
        p.push(d3.selectAll('rect')[0][m]['id']);
        p.push(d3.selectAll('rect')[0][m]['x']['baseVal'].value);
        p.push(d3.selectAll('rect')[0][m]['y']['baseVal'].value);
        p.push(d3.selectAll('rect')[0][m]['width']['baseVal'].value);
        p.push(d3.selectAll('rect')[0][m]['height']['baseVal'].value);
        initPosition.push(p);
    }
    resetfilterByViews();
    query();
    recordPathUpdate();
}