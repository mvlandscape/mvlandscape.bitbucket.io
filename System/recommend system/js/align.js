function align(direction){
    if(shift_chosed_list.size>1)
        choosed_rect_set = shift_chosed_list;
    else
        choosed_rect_set = lasso_rect_set;
    
    if(choosed_rect_set.size == 0)
    {
        alert("Please select Rectangles to be aligned first!");
        return;
    }

    changeRectStyle();
    d3.selectAll('circle').remove();
    d3.selectAll('#lineHint').remove();
    // d3.selectAll('rect').style('stroke', 'none');


    let sorted_rects = sort(direction, choosed_rect_set);

    if(direction == 'top' || direction ==='bottom'){
        let rect_0 = sorted_rects[0];
        let rect_n = sorted_rects[sorted_rects.length - 1];


        let sum_width = 0, bb_top;
        if(direction === 'top'){
            bb_top = Number.MAX_VALUE;
            for(let rect of sorted_rects){
                sum_width += (rect[1].right - rect[1].left);
                bb_top = Math.min(bb_top, rect[1].top);
            }
        }
        if(direction === 'bottom'){
            bb_top = Number.MIN_VALUE;
            for(let rect of sorted_rects){
                sum_width += (rect[1].right - rect[1].left);
                bb_top = Math.max(bb_top, rect[1].bottom);
            }
        }

        let bb_x = rect_0[1].left, bb_z = rect_n[1].right;
        let shift_x = Math.max(0, sum_width - (bb_z - bb_x));
        // let min_x,max_x;
        // if(direction === 'top')
        // {
        //     min_x = 
        // }
        let min_x = bb_x, max_x = bb_z + shift_x;
        let left_bound = min_x, right_bound = max_x;
        let fixed_len = 0;

        for(let i = 0; i < sorted_rects.length; i++){
            if(direction === 'top')
                left_bound = reposition(sorted_rects[i], left_bound, right_bound, bb_top,'top');
            else 
                left_bound = reposition(sorted_rects[i], left_bound, right_bound, bb_top - sorted_rects[i][1].bottom +sorted_rects[i][1].top ,'top');
            fixed_len += sorted_rects[i][1].right - sorted_rects[i][1].left;
            right_bound = max_x - (sum_width - fixed_len);
        }
    }

    if(direction == 'left' || direction ==='right'){
        let rect_0 = sorted_rects[0];
        let rect_n = sorted_rects[sorted_rects.length - 1];



        let sum_width = 0, bb_top;
        if(direction === 'left'){
            bb_top = Number.MAX_VALUE;
            for(let rect of sorted_rects){
                sum_width += (rect[1].bottom - rect[1].top);
                bb_top = Math.min(bb_top, rect[1].left);
            }
        }
        if(direction === 'right'){
            bb_top = Number.MIN_VALUE;
            for(let rect of sorted_rects){
                sum_width += (rect[1].bottom - rect[1].top);
                bb_top = Math.max(bb_top, rect[1].right);
            }
        }

        let bb_x = rect_0[1].top, bb_z = rect_n[1].bottom;
        let shift_x = Math.max(0, sum_width - (bb_z - bb_x));

        let min_x = bb_x, max_x = bb_z + shift_x;
        let left_bound = min_x, right_bound = max_x;
        let fixed_len = 0;

        for(let i = 0; i < sorted_rects.length; i++){
            if(direction === 'left')
                left_bound = reposition(sorted_rects[i], left_bound, right_bound, bb_top,'left');
            else 
                left_bound = reposition(sorted_rects[i], left_bound, right_bound, bb_top - sorted_rects[i][1].right +sorted_rects[i][1].left ,'left');
            fixed_len += sorted_rects[i][1].bottom - sorted_rects[i][1].top;
            right_bound = max_x - (sum_width - fixed_len);
        }
    }


    resetfilterByViews();
    query();

    recordPathUpdate();

}

function sort(direction, rect_set){
    let array_obj = Array.from(rect_set);
    array_obj.sort(function(a, b){
        let a_comp = comp(direction, a);
        let b_comp = comp(direction, b);

        if(a_comp < b_comp){
            return -1;
        } else if (a_comp > b_comp){
            return 1;
        }

        return 0;
    });

    return array_obj;
}

function comp(direction, rect){
    if (direction == 'top' || direction == 'bottom') {
       return rect[1].left;
    } else
        return rect[1].top;
}

function reposition(obj, left_bound, right_bound, y ,direction){
    let key = obj[0];
    let rect = obj[1];
    let x_view,y_view;
    if(direction === 'top'){
        x_view = Math.max(rect.left, left_bound);
        x_view = Math.min(x_view, right_bound);
        y_view = y;
    }

    if(direction === 'left'){
        y_view = Math.max(rect.top, left_bound);
        y_view = Math.min(y_view, right_bound);
        x_view = y;
    }



    d3.select('#' + key).attr('x', x_view).attr('y', y_view);
    d3.select('#' + key + 'R').attr('x', x_view).attr('y', y_view);
    let aRectShape = new RectShape(x_view,y_view,x_view+rect.right-rect.left,y_view+rect.bottom-rect.top)
    rectPosition.set(key,aRectShape)
    if(shift_chosed_list.size>1)
        shift_chosed_list.set(key,aRectShape)
    else
        lasso_rect_set.set(key,aRectShape)
    if(direction === 'top'){
        return x_view + (rect.right - rect.left);
    }else if(direction === 'left'){
        return y_view + (rect.bottom - rect.top);
    }
    

}